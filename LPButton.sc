LPButton {
    classvar <>midiout, <>uid;

    var <addr, <type, <onFunc, <offFunc, <toggle, <onColor, <offColor;
    var <focus=true, <active=false, <midiFunc, <noteOnFunc, <noteOffFunc;
    var color;

    *new { |addr=0, type=\note, onFunc, offFunc, toggle=false, onColor=\redhi, offColor=\redlo| 
        ^super.newCopyArgs(addr, type, onFunc, offFunc, toggle, onColor, offColor).init;
    }

    init { 

        color = ( 
            \off: 12,
            \redlo: 13,
            \redhi: 15,
            \amberlo: 29,
            \amberhi: 63,
            \yellow: 62,
            \greenlo: 28,
            \greenhi: 60
        ); 

        if(type==\note){
            if(toggle){
                this.noteToggleFunc;
            }{
                this.noteOneShotFunc;
            }
        };

        if(type==\cc){
            if(toggle){
                this.ccToggleFunc;
            }{
                this.ccOneShotFunc;
            } 
        }; 
    }

    active_ {|val|
        active = val;
        if (val){
            if((type==\note) && toggle){
                onFunc.();
                this.setNoteColor(addr, onColor);
            };
            if((type==\cc) && toggle){
                onFunc.();
                this.setControlColor(addr, onColor);
            };
        }{
            if((type==\note) && toggle){
                offFunc.();
                this.setNoteColor(addr, offColor);
            };
            if((type==\cc) && toggle){
                offFunc.();
                this.setControlColor(addr, offColor);
            };
        }
    }

    focus_ {|val|
        if(val!=focus){
            focus = val;
            if(focus){
                if((type==\note) && toggle.not){
                    this.noteOneShotFunc;
                };
                if((type==\note) && toggle){
                    this.noteToggleFunc;
                    if(active){
                        this.setNoteColor(addr, onColor);
                    }{
                        this.setNoteColor(addr, offColor);
                    }
                };
                if((type==\cc) && toggle.not){
                    this.ccOneShotFunc;
                }; 
                if((type==\cc) && toggle){
                    this.ccToggleFunc;
                    if(active){
                        this.setControlColor(addr, onColor);
                    }{
                        this.setControlColor(addr, offColor);
                    } 
                }; 
            }{
                if(type==\note){
                    midiFunc.free;
                    this.setNoteColor(addr, \off);
                };
                if(type==\cc){
                    midiFunc.free;
                    this.setControlColor(addr, \off);
                }; 
            };
        }
    }

    setControlColor {|a, c|
        midiout.control(0, a, color[c]);
    }

    setNoteColor {|a, c|
        midiout.noteOn(0, a, color[c]);
    }

    noteOneShotFunc {
        midiout.noteOn(0, addr, color[offColor]);
        noteOnFunc = MIDIFunc.noteOn({|val, num|
            onFunc.(val, num);
            this.setNoteColor(addr, onColor);
        }, addr, 0, uid);

        noteOffFunc = MIDIFunc.noteOff({|val, num|
            offFunc.(val, num);
            this.setNoteColor(addr, offColor);
        }, addr, 0, uid);
    }

    noteToggleFunc {
        midiout.noteOn(0, addr, color[offColor]);
        midiFunc = MIDIFunc.noteOn({|val, num, chan|
            if (active) {
                offFunc.(val, num);
                this.setNoteColor(addr, offColor);
                active = false;
            }{
                onFunc.(val, num);
                this.setNoteColor(addr, onColor);
                active = true;
            };
        }, addr, 0, uid);
    }

    ccOneShotFunc {
        midiout.control(0, addr, color[offColor]);
        midiFunc = MIDIFunc.cc({|val, num, chan|
            if (val==0) {
                offFunc.(val, num);
                this.setControlColor(addr, offColor);
            }{
                onFunc.(val, num);
                this.setControlColor(addr, onColor);
            };
        }, addr, 0, uid);
    }

    ccToggleFunc {
        midiout.control(0, addr, color[offColor]);
        midiFunc = MIDIFunc.cc({|val, num|
            if (active) {
                offFunc.(val, num);
                this.setControlColor(addr, offColor);
                active = false;
            }{
                onFunc.(val, num);
                this.setControlColor(addr, onColor);
                active = true;
            };
        }, addr, 0, uid, argTemplate: 127);
    }

    free {
        midiFunc.free();
        this.setControlColor(addr, \off);
        offFunc.();
        noteOnFunc.free();
        noteOffFunc.free();
        this.setNoteColor(addr, \off);
    }
}
